import os
import pandas as pd 


# Si existant, récupérer le fichier de logs précédent
if os.path.exists("/opt/airflow/dags/exemple/ex_logs/ex_logs_TC.csv") :

	logs = pd.read_csv("/opt/airflow/dags/exemple/ex_logs/ex_logs_TC.csv", sep = ";", decimal = ',')

# Sinon créer un dataframe vide avec les variables souhiatées
else :
	logs = pd.DataFrame({
		"task_id" : [],
		"start" : [],
		"end" :[]
	})




# Logs d'échec
new_logs = pd.DataFrame({
	"task_id" : ["execute_python"],
	"start" : [pd.NA],
	"end" : [pd.NA],
	"status" : "failed"})


# Compil avec les précédents logs
all_logs = pd.concat([logs, new_logs])


# Exports
all_logs.to_csv("/opt/airflow/dags/exemple/ex_logs/ex_logs_TC.csv",
	sep = ';' , decimal = ',', index = False)